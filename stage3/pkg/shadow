#!/bin/bash

hvconfig_pre()
{
    CONFIGURE_OPTS+=" \
        --without-selinux"

    cd ${SRC_DIR}

    # Disable the installation of the groups program and its man page, as
    # Coreutils provides a better version.
    sed -i 's/groups$(EXEEXT) //' src/Makefile.in
    find man -name Makefile.in -exec sed -i 's/groups\.1 / /' {} \;

    # Instead of using the default crypt method, use the more secure SHA-512
    # method of password encryption, which also allows passwords longer than 8
    # characters. It is also necessary to change the obsolete /var/spool/mail
    # location for user mailboxes that Shadow uses by default to the /var/mail
    # location used currently:
    sed -i -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD SHA512@' \
        -e 's@/var/spool/mail@/var/mail@' /etc/login.defs
}

hvbuild_post()
{
    # Expands PATH to /usr/local/bin for normal and root user and to
    # /usr/local/sbin for root user only:
    sed -i -e 's@PATH=/sbin:/bin:/usr/sbin:/usr/bin@&:/usr/local/sbin:/usr/local/bin@' \
        -e 's@PATH=/bin:/usr/bin@&:/usr/local/bin@' /etc/login.defs

    # Move some misplaced symlinks/programs to their proper locations.
    mv /usr/bin/passwd /bin

    # Disable creation of mailbox files by useradd:
    sed -i 's/yes/no/' /etc/default/useradd

    # Configure Linux-PAM:
    install -v -m644 /etc/login.defs /etc/login.defs.orig
    for FUNCTION in FAIL_DELAY FAILLOG_ENAB \
        LASTLOG_ENAB \
        MAIL_CHECK_ENAB \
        OBSCURE_CHECKS_ENAB \
        PORTTIME_CHECKS_ENAB \
        QUOTAS_ENAB \
        CONSOLE MOTD_FILE \
        FTMP_FILE NOLOGINS_FILE \
        ENV_HZ PASS_MIN_LEN \
        SU_WHEEL_ONLY \
        CRACKLIB_DICTPATH \
        PASS_CHANGE_TRIES \
        PASS_ALWAYS_WARN \
        CHFN_AUTH ENCRYPT_METHOD \
        ENVIRON_FILE
    do
        sed -i "s/^${FUNCTION}/# &/" /etc/login.defs
    done

    install -v -m644 ${SCRDIR}/misc/shadow-pam.d/* /etc/pam.d/

    for PROGRAM in chfn chgpasswd chpasswd chsh groupadd groupdel \
        groupmems groupmod newusers useradd userdel usermod
    do
        install -v -m644 /etc/pam.d/chage /etc/pam.d/${PROGRAM}
        sed -i "s/chage/$PROGRAM/" /etc/pam.d/${PROGRAM}
    done

    # Configuring Login Access
    # Instead of using the /etc/login.access file for controlling access to the
    # system, Linux-PAM uses the pam_access.so module along with the
    # /etc/security/access.conf file. Rename the /etc/login.access file:
    [ -f /etc/login.access ] && mv -v /etc/login.access{,.NOUSE}

    # Configuring Resource Limits
    # Instead of using the /etc/limits file for limiting usage of system
    # resources, Linux-PAM uses the pam_limits.so module along with the
    # /etc/security/limits.conf file. Rename the /etc/limits file:
    [ -f /etc/limits ] && mv -v /etc/limits{,.NOUSE}

    # Set system group ID range from 1 to 90:
    #    0 is reserved for group 'root'
    #   99 is reserved for group 'nogroup'
    sed -e "s/\(SYS_UID_MIN\s\+\)[0-9]\+/\1  1/" \
        -e "s/\(SYS_UID_MAX\s\+\)[0-9]\+/\1 90/" \
        -e "s/\(SYS_GID_MIN\s\+\)[0-9]\+/\1  1/" \
        -e "s/\(SYS_GID_MAX\s\+\)[0-9]\+/\1 90/" \
        -i /etc/login.defs
}
