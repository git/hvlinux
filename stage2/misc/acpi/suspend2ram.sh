#!/bin/sh

case "$1" in
    button-sleep)
        logger "Sleep button pressed"
        ;;
    button-lid)
        if [ -f /proc/acpi/button/lid/LID/state ]; then
            if grep -q open /proc/acpi/button/lid/LID/state; then
                logger "Lid opened detected, do nothing"
                # Do not act on lid opened event. Only on lid closed.
                exit 0
            fi

            logger "Lid closed detected"
        fi
        ;;
    *)
        logger "Unknown event: ${1}"
        ;;
esac

logger "Going to sleep"

XSCREENSAVER_ACTIVE="$(pidof /usr/bin/xscreensaver)"

# Check if xscreensaver is running. if not, just skip on.
if [ "x${XSCREENSAVER_ACTIVE}" != "x" ]; then
    # Run the lock command as the user who owns xscreensaver process,
    # and not as root, which won't work.
    su "$(ps aux | grep xscreensaver | grep -v grep | grep ${XSCREENSAVER_ACTIVE} | awk '{print $1}' )" \
        -c "/usr/bin/xscreensaver-command -lock" &
    sleep 1
fi

# discover video card's ID
ID=`lspci | grep VGA | awk '{ print $1 }' | sed -e 's@0000:@@' -e 's@:@/@'`

# securely create a temporary file
TMP_FILE=`mktemp /var/tmp/video_state.XXXXXX`
trap 'rm -f $TMP_FILE' 0 1 15

# write all unwritten data (just in case)
sync

# dump current data from the video card to the
# temporary file
cat /proc/bus/pci/$ID > $TMP_FILE

echo "Entering suspend mode"

echo -n mem > /sys/power/state

echo "Resuming"

# restore video card data from the temporary file
# on resume
cat $TMP_FILE > /proc/bus/pci/$ID

# remove temporary file
rm -f $TMP_FILE
