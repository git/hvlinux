#!/bin/sh

# ifup

DEVICE=${1}

BRCTL=/usr/sbin/brctl

WPA_SUPP_OPTS="-Dnl80211 -B -s -c /etc/wpa_supplicant.conf"

# Source functions library
source /etc/rc.d/init.d/functions

log_script_name "$0 $*"

# Load network interface card parameters
source /etc/sysconfig/network/ifconfig.${DEVICE}

# Load DHCP client parameters
source /etc/sysconfig/network/dhcp-client

# Make sure DHCP client is available.
if [ ! -x ${DHCP_PROG} ]; then
    echo "Program \"${DHCP_PROG}\" is not executable."
    exit ${EXIT_CODE_FAILURE}
fi

# Check that the brctl program is present and executable.
check_brctl()
{
    if [ ! -x ${BRCTL} ]; then
        msg_log "*** ERROR: /usr/sbin/brctl not found."
        exit ${EXIT_CODE_FAILURE}
    fi
}

# Bring interface up
bring_if_up()
{
    link_status=$(ip link show ${DEVICE})
    if [ -n "${link_status}" ]; then
        if ! echo "${link_status}" | grep -q UP; then
            cmd_run_log ip link set ${DEVICE} up
        fi
    else
        echo "Interface ${DEVICE} not found"
        exit ${EXIT_CODE_WARNING}
    fi

    # Check if a cable is plugged for wired interface
    if echo "${DEVICE}" | grep -q "eth" ; then
        count=0
        while cat /sys/class/net/${DEVICE}/carrier | grep -q "0"; do
            echo "Waiting for carrier to go up"
            sleep 0.25

            let count=count+1
            # 2.5 seconds delay
            if [ $count -gt 10 ]; then
                echo "Interface ${DEVICE}: carrier not detected (cable unplugged?)"
                exit ${EXIT_CODE_WARNING}
            fi
        done
    fi
}

# Static IP address protocol
proto_static()
{
    cmd_run_log ip addr add ${IPADDR}/${NETMASK_LENGTH} dev ${DEVICE} \
        brd + ${IFSCOPE} || exit ${EXIT_CODE_FAILURE}

    # Bring interface up
    bring_if_up

    if [ -n "${GATEWAY}" ]; then
        cmd_run_log ip route add default via ${GATEWAY} || \
            exit ${EXIT_CODE_FAILURE}
    fi
}

# Obtain IP address from DHCP
proto_dhcp()
{
    # Bring interface up
    bring_if_up

    cmd_run_log ${DHCP_PROG} ${DHCP_START} ${DEVICE}
}

proto_wireless()
{
    # Bring interface up
    bring_if_up

    # Check if a socket exists, meaning wpa_supplicant is running
    if [ -S /var/run/wpa_supplicant/${DEVICE} ]; then
        echo "wpa_supplicant already running"

        # Make sure wpa_supplicant is responding
        if ! wpa_cli -i ${DEVICE} status; then
            echo "Stopping unresponding wpa_supplicant"
            killall wpa_supplicant
            rm /var/run/wpa_supplicant/${DEVICE}
        fi
    fi

    # Start wpa_supplicant if it is not running
    if [ ! -S /var/run/wpa_supplicant/${DEVICE} ]; then
        echo "Starting wpa_supplicant"
        wpa_supplicant ${WPA_SUPP_OPTS} -i ${DEVICE}
    fi

    count=0
    while ! wpa_cli -i ${DEVICE} status | grep "wpa_state=COMPLETED"; do
        echo "Waiting for wpa_supplicant to complete"
        sleep 0.25

        let count=count+1
        if [ $count -gt 20 ]; then
            echo "wpa_supplicant failure"
            exit ${EXIT_CODE_WARNING}
        fi
    done

    # Obtain SSID that wpa_supplicant connected to, if applicable
    SSID=$(wpa_cli -i ${DEVICE} status | egrep "^ssid=" | \
        sed 's!^ssid=\(.*\)!\1!')

    # Convert spaces to underscores in SSID field
    SSID=$(echo ${SSID} | sed -e "s!\ !_!g")

    echo "wpa_supplicant connected to network \"${SSID}\""

    if [ -z "${SSID}" ]; then
        echo "Not connected to any network"
        exit ${EXIT_CODE_WARNING}
    fi

    if [ ! -d /etc/sysconfig/network/ssid ]; then
        echo "Missing \"/etc/sysconfig/network/ssid\" directory"
        exit ${EXIT_CODE_WARNING}
    fi

    # Warning:
    # wpa_supplicant will connect to a given network SSID even if the case is
    # incorrect (MyNetwork and MYNETWORK will work). But the ssid network file
    # configuration case must match exactly what was returned by wpa_supplicant.
    if [ ! -f "/etc/sysconfig/network/ssid/${SSID}" ]; then
        echo "${0}: Missing network configuration file \"/etc/sysconfig/network/ssid/${SSID}\""
        exit ${EXIT_CODE_FAILURE}
    fi

    source "/etc/sysconfig/network/ssid/${SSID}"
}

# Make sure interface is available
if [ ! -d /sys/class/net/${DEVICE} ]; then
    echo "Interface ${DEVICE} not found"
    exit ${EXIT_CODE_WARNING}
fi

# Determining if the interface is a bridge:
if [ "x${BRIDGE}" = "xyes" ]; then
    check_brctl
    cmd_run_log ${BRCTL} addbr ${DEVICE} &&
    cmd_run_log ${BRCTL} stp ${DEVICE} off || exit 1
fi

# Determining if the interface is part of a bridge:
if [ -n "${BRIDGE_TO}" ]; then
    check_brctl
    cmd_run_log ${BRCTL} addif ${BRIDGE_TO} ${DEVICE} || exit 1
fi

if [ "x${BOOTPROTO}" = "xwifi" ]; then
    proto_wireless
    # At this point, the SSID script has been sourced, and its
    # BOOTPROTO variable will contain either "static" or "dhcp",
    # so configuration will continue below.
fi

if [ "x${BOOTPROTO}" = "xdhcp" ]; then
    proto_dhcp
elif [ "x${BOOTPROTO}" = "xstatic" ]; then
    proto_static
elif [ x${BOOTPROTO} = "xpppoe" ]; then
    cmd_run_log pppoe-start
else
    echo "Invalid BOOTPROTO variable"
    exit 1
fi

exit $?
