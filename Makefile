# Makefile for hvlinux

# Idées pour future targets:
#   clean
#   strip
#   compress -> compressdoc (à partir du stage2)
#   dep
#   list -> list installed packages and versions

.PHONY: get test

RUN_MODE := $(shell . functions/version && check_hvlinux_version)

ifeq ($(RUN_MODE),HOST)
  STAGES := 0 1
else ifeq ($(RUN_MODE),NATIVE)
  STAGES := 2 3 4 5
else
  $(error Unable to determine run mode.)
endif

ifeq ($(MAKECMDGOALS),get)
  STAGES := $(shell seq 0 5)
endif
ifeq ($(MAKECMDGOALS),test)
  STAGES := $(shell seq 0 5)
endif

all:
	@for k in $(STAGES); do \
	  make -C stage$${k} $(MAKECMDGOALS) || exit 1; \
        done

$(MAKECMDGOALS): all
