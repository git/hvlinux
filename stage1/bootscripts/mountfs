#!/bin/bash

# mountfs

# Source functions library
source /etc/rc.d/init.d/functions

log_script_name "$0 $*"

case "$1" in
    start)
	# Remount the root partition in read-write mode.
	cmd_run_log_box "Remounting root file system Read/Write" \
	    mount -o remount,rw /

        # Remove the possible /fastboot and /forcefsck files. they are only
        # supposed to be used during the next reboot's checkfs which just
        # happened. If you want to fastboot or forcefsck again you'll have to
        # recreate the files
        rm -f /fastboot /forcefsck

        # Walk through /etc/fstab and mount all file systems that don't have
	# the noauto option set in the fs_mntops field (the 4th field. See man
	# fstab for more info).
	cmd_run_log_box "Mounting other file systems" mount -a -O no_netdev
        ;;

    stop)
        # Unmount all the file systems, mounting the root file system
        # read-only (all are unmounted but because root can't be unmounted
        # at this point mount will automatically mount it read-only which
        # is what is supposed to happen. This way no data can be written
	# anymore to disk).
	cmd_run_nolog_box "Unmounting file systems" \
	    umount -a -d -r -v -t notmpfs,nosysfs,nodevtmpfs,noproc,nodevpts
        if [ $? -ne 0 ]; then
            exit ${EXIT_CODE_WARNING}
        fi

        # Make sure / is mounted read-only (umount bug?)
        mount -o remount,ro /
        ;;

    *)
        echo "Usage: $0 {start|stop}"
        exit ${EXIT_CODE_FAILURE}
        ;;
esac

exit $?
