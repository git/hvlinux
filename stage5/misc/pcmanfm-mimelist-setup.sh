#!/bin/bash

# Create a list of mimetypes for PCManFM.

MIMEAPPS_FILE="${HOME}/.local/share/applications/mimeapps.list"

# Arg 1: Desktop file prefix
function add_mime_association()
{
    if [ ${#} -ne 1 ]; then
        echo "Missing application name"
        exit 1
    fi

    local APP=${1}
    local DESKTOP_FILE=/usr/share/applications/${APP}.desktop

    if [ -f ${DESKTOP_FILE} ]; then
        for t in $(cat ${DESKTOP_FILE} | grep MimeType | \
            sed 's!^MimeType=!!' | tr -s ";" "\n"); do
            echo "${t}=${APP}.desktop;" >> ${MIMEAPPS_FILE}
        done
    fi
}

cat > ${MIMEAPPS_FILE} << EOF
[Added Associations]
application/pdf=evince.desktop;
application/postscript=evince.desktop;
application/x-dvi=evince.desktop;
application/msword=abiword.desktop;
EOF

add_mime_association "viewnior"
add_mime_association "vlc"
add_mime_association "xdvi"
add_mime_association "gnumeric"
add_mime_association "inkscape"
add_mime_association "emacs"
add_mime_association "transmission"
add_mime_association "xarchiver"
