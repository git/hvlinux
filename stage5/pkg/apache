#!/bin/bash

CONFDIR=/etc/httpd

hvconfig_pre()
{
    cat >> ${SRC_DIR}/config.layout << "EOF" &&
# BLFS FHS layout
<Layout FHS>
    prefix:          /usr
    exec_prefix:     ${prefix}
    bindir:          ${exec_prefix}/bin
    sbindir:         ${exec_prefix}/sbin
    libdir:          ${exec_prefix}/lib
    libexecdir:      ${exec_prefix}/libexec/apache
    mandir:          ${prefix}/share/man
    sysconfdir:      /etc/httpd
    datadir:         /srv/www
    installbuilddir: ${libexecdir}/build
    errordir:        ${datadir}/error
    iconsdir:        ${datadir}/icons
    htdocsdir:       ${datadir}/htdocs
    manualdir:       ${datadir}/manual
    cgidir:          ${datadir}/cgi-bin
    includedir:      ${prefix}/include/apache
    localstatedir:   ${datadir}
    runtimedir:      /var/run
    logfiledir:      /var/log/apache
    proxycachedir:   /var/cache/apache/proxy
</Layout>
EOF

    groupadd --system -f ${HTTPD_USER}
    hv_useradd --system -c WebServer -d /dev/null -g ${HTTPD_USER} \
        -s /bin/false ${HTTPD_USER}

    CONFIGURE_OPTS=" \
        --enable-layout=FHS \
        --enable-mods-shared=all \
        --enable-ssl \
        --with-z"
}

hvbuild_post()
{
    chown -v root:root /usr/bin/{apxs,dbmmanage} \
        /usr/sbin/{apachectl,envvars{,-std}} \
        /usr/libexec/apache/httpd.exp \
        /usr/share/man/man1/{ab,apxs,dbmmanage,ht{dbm,digest,passwd,txt2dbm},logresolve}.1 \
        /usr/share/man/man8/{apachectl,htcacheclean,httpd,rotatelogs,suexec}.8 &&

    sed -i -e "s!^\(DocumentRoot\) .*!\1 /srv/www/htdocs!" /etc/httpd/httpd.conf

    # Modifying the listening port if an alternate one is specified.
    if [ -n "${HTTPD_PORT}" ]; then
        if ! grep -q "Listen ${HTTPD_PORT}" ${CONFDIR}/httpd.conf; then
            # Adding alternate port to default port of 80
	    sed -i -e "s%\(Listen 80\)%\1\nListen ${HTTPD_PORT}%" \
                ${CONFDIR}/httpd.conf
        fi
    fi

    # Modifying the configuration file to run the server as a dedicated user:
    sed -i \
        -e "s!^\(User\).*!\1 ${HTTPD_USER}!g" \
        -e "s!^\(Group\).*!\1 ${HTTPD_USER}!g" \
        ${CONFDIR}/httpd.conf

    # SSL Certificates generation script
    cat > ${CONFDIR}/generate-ssl-certs << "EOF"
#!/bin/bash

# Creation of SSL directories for Apache
cd ${CONFDIR}
mkdir -p ssl

cd ssl

echo "-------------------------------------------------"
echo "Generation of the RSA private key"
echo "-------------------------------------------------"
openssl genrsa -des3 -rand file1:file2:file3:file4:file5 -out server.key 1024

echo "-------------------------------------------------"
echo "Removing pass-phrase from the private key"
echo "-------------------------------------------------"
cp server.key server.key.orig
openssl rsa -in server.key.orig -out server.key
# The unencrypted key must be readable only by root!
chown root:root server.*
chmod 0400 server.*

echo "-------------------------------------------------"
echo "Generation of a Certificate Signing Request (CSR)"
echo ""
echo "Example fields:"
echo "Country name:             CA"
echo "State or Province Name:   Quebec"
echo "Locality Name:            Montreal"
echo "Organization Name:        Hugo Villeneuve"
echo "Organizational Unit Name: ."
echo "Common Name:              www.hugovil.com"
echo "Email Address:            webmaster@hugovil.com"
echo ""
echo "Please enter the following 'extra' attributes"
echo "to be sent with your certificate request"
echo "A challenge password []:     (press enter)   "
echo "An optional company name []: (press enter)   "
echo ""
echo "-------------------------------------------------"

openssl req -new -key server.key -out server.csr

echo "-------------------------------------------------"
echo "Generation of a self-signed certificate"
echo "-------------------------------------------------"
openssl x509 -req -days 10000 -in server.csr -signkey server.key -out server.crt

exit $?
EOF

    chmod 740 ${CONFDIR}/generate-ssl-certs

    mkdir -p ${CONFDIR}/ssl
    cp ${CONFDIR}/extra/httpd-ssl.conf ${CONFDIR}/ssl/ssl.conf

    sed -i -e "s!^<IfModule ssl_module>!<IfModule ssl_module>\n  Include ${CONFDIR}/ssl/ssl.conf!" ${CONFDIR}/httpd.conf

    mkdir -p /srv/www/htdocs/ssl
    sed -i -e "s!^DocumentRoot .*!DocumentRoot /srv/www/htdocs/ssl!" ${CONFDIR}/ssl/ssl.conf
    sed -i -e "s!^ServerName .*:443!ServerName www.${DOMAIN}:443!" ${CONFDIR}/ssl/ssl.conf
    sed -i -e "s!^ServerAdmin .*!ServerAdmin webmaster@${DOMAIN}!" ${CONFDIR}/ssl/ssl.conf
    sed -i -e "s!${CONFDIR}!${CONFDIR}/ssl!" ${CONFDIR}/ssl/ssl.conf

    chown -v -R ${HTTPD_USER}:${HTTPD_USER} /srv/www

    # Bootscript
    install -v -m740 ${SCRDIR}/bootscripts/apache /etc/rc.d/init.d

    # script-name start stop
    if [ "x${DEFAULT_WEB_SERVER}" == "xhttpd" ]; then
        bootscript_add_rc3 apache 70 20
    fi
}
