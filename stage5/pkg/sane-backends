#!/bin/bash

hvconfig_pre()
{
    local USE_X_OPTION

    if [ x${INST_TYPE} = "xworkstation" ]; then
        USE_X_OPTION="yes"
    else
        USE_X_OPTION="no"
    fi

    # Add group for UDEV rules
    groupadd --system -f scanner

    # Group for sane server
    hv_useradd --system -c saned -d /dev/null -g scanner -s /bin/false saned

    CONFIGURE_OPTS+=" \
        --with-x=${USE_X_OPTION} \
        --localstatedir=/var \
        --with-group=scanner \
        --enable-libusb_1_0"
}

hvbuild_post()
{
    install -v -m 644 tools/udev/libsane.rules \
        /etc/udev/rules.d/65-scanner.rules

    chgrp -v scanner /var/lock/sane

    # Addition to /etc/services (for both the client and server)
    string_add "sane      6566/tcp   saned # SANE Network Scanner Daemon" \
        /etc/services

    # Creating '/etc/sane.d/dll.conf' and adding entry 'net'
    echo "net" > /etc/sane.d/dll.conf

    if [ -z "${SANE_SERVER}" ]; then
        # If 'SANE_SERVER' is not defined, then we automatically configure for
        # the SANE Network Daemon.

        # Adding entry to '/etc/sane.d/dll.conf'
        #######echo "${SANE_DRIVER}" >> /etc/sane.d/dll.conf

        # Creating '/etc/sane.d/saned.conf'
        echo "${LAN_NETWORK_MASK}" > /etc/sane.d/saned.conf

        # Addition to xinetd
        cat > /etc/xinetd.d/saned << "EOF"
service sane
{
   disable        = no
   socket_type    = stream
   protocol       = tcp
   wait           = no
   user           = saned
   group          = scanner
   server         = /usr/sbin/saned
   log_type       = SYSLOG local4 info
}
EOF
    else
        # If 'SANE_SERVER' is defined, then we must specify the address of the
        # remote SANE server to use.
        echo "${SANE_SERVER}" > /etc/sane.d/net.conf
    fi
}
