#!/bin/bash

# Fetch updated version numbers, but do not import them into the
# repository. Like patches, they must be imported manually.

USE_LFS_PATCHES=1
USE_BLFS_PATCHES=1

source ../functions/main

update_packages_init ${*}

UNWANTED_VIDEO_DRIVERS="ark ast cirrus dummy glint i128 mach64 mga neomagic \
    r128 savage siliconmotion sis tdfx tga trident v4l voodoo \
    vmware newport xgixp geode"

UNWANTED_INPUT_DRIVERS="void"

XORG_MODULES_LIST="${XORG_APP} ${XORG_DRIVER} ${XORG_FONT} ${XORG_LIB} \
    ${XORG_PROTO}"

# Each module has a dedicated wget and md5 file.
for module in ${XORG_MODULES_LIST}; do
    MOD_NAME=$(get_pkg_name ${module})
    DEST_DIR=${LFS_PKG_DIR}/versions

    fpkg -d ${DEST_DIR} -e wget -w ${module} ${BLFS_XORG_URL}
    fpkg -d ${DEST_DIR} -e md5  -w ${module} ${BLFS_XORG_URL}

    if [ "${MOD_NAME}" = "driver" ]; then
        # Exclude some unwanted video drivers
        for ext in wget md5; do
            for unwanted in ${UNWANTED_VIDEO_DRIVERS}; do
                sed -i -e "/.*xf86-video-${unwanted}.*/d" \
                    ${DEST_DIR}/${module}.${ext}
            done
        done

        # Exclude some unwanted input drivers
        for ext in wget md5; do
            for unwanted in ${UNWANTED_INPUT_DRIVERS}; do
                sed -i -e "/.*xf86-input-${unwanted}.*/d" \
                    ${DEST_DIR}/${module}.${ext}
            done
        done

        # Always install synaptics driver
        for ext in wget md5; do
            sed -e "s/#\(.*xf86-input-synaptics.*\)/\1/" \
                -i ${DEST_DIR}/${module}.${ext}
        done
    fi

    if [ "${MOD_NAME}" = "app" ]; then
        # Remove unneeded apps
        for ext in wget md5; do
            sed -i -e '/.*twm-.*/d'    ${DEST_DIR}/${module}.${ext}
            sed -i -e '/.*xclock-.*/d' ${DEST_DIR}/${module}.${ext}
        done
    fi

    # Remove all commented lines for clarity
    for ext in wget md5; do
        sed -i -e '/^#.*/d'    ${DEST_DIR}/${module}.${ext}
    done
done

exit ${?}
