#!/bin/bash

source ../functions/main

update_packages_init ${*}

lpkg     2 ${PYTHON}
lpkg     2 ${GLIB}

XORG_MODULES_LIST="app driver font lib proto"

VERSIONS_DIR="${SCRDIR}/versions"

for module in ${XORG_MODULES_LIST}; do
    MOD_NAME=$(get_pkg_name ${module})

    SUBMODS="$(grep -v '^#' ${VERSIONS_DIR}/${module}.md5 | \
        awk '{print $2}' | \
        sed 's!\.tar\.bz2$!!')"

    for submod in ${SUBMODS}; do
        fpkg -m xorg -s ${MOD_NAME} ${submod}
    done

    pushd ${LFS_PKG_DIR} >> ${LFS_LOG_FILE}
    md5sum -c ${VERSIONS_DIR}/${module}.md5 >> ${LFS_LOG_FILE}
    if [ $? -ne 0 ]; then
        echo "  md5sum error for ${module}"
    fi
    popd >> ${LFS_LOG_FILE}
done

fpkg -m xorg -s util ${XORG_UTIL_MACROS}
fpkg -m xorg -s lib ${LIBXAU}
fpkg -m xorg -s lib ${LIBXDMCP}
fpkg -m sf -e "tgz" -f "${MOTIF}-src" ${MOTIF}
fpkg -m xorg -s data ${XBITMAPS}

fpkg -m fd -o "xcb" -s "dist" ${LIBPTHREAD_STUBS}
fpkg -m fd -o "xcb" -s "dist" ${XCB_PROTO}
fpkg -m fd -o "xcb" -s "dist" ${LIBXCB}
fpkg -m fd -o "xcb" -s "dist" ${XCB_UTIL}
fpkg -m fd -o "xcb" -s "dist" ${XCB_UTIL_KSYMS}

fpkg ${LIBDRM} "http://dri.freedesktop.org/libdrm"
fpkg -e "tar.gz" ${TALLOC} "http://samba.org/ftp/talloc"
fpkg -e "tar.gz" -f "${LLVM}.src" -v 0 ${LLVM} "http://llvm.org/releases"
fpkg -v 0 ${MESALIB} "ftp://ftp.freedesktop.org/pub/mesa"
fpkg ${GLU} "ftp://ftp.freedesktop.org/pub/mesa/glu"
fpkg -m sf ${FREEGLUT}
fpkg -m xorg -s data ${XCURSOR_THEMES}
fpkg -s data/xkeyboard-config ${XKEYBOARD_CONFIG} ${XORG_URL}

fpkg -e "tar.gz" ${PIXMAN} ${CAIRO_URL}

fpkg -m fd -s "releases/dbus" ${DBUS}
fpkg -m gnome ${GOBJECT_INSTROSPECTION}
fpkg -m fd -o "dbus" -s "releases/dbus-glib" ${DBUS_GLIB}

fpkg ${LIBEPOXY} "http://crux.nu/files"
fpkg -e "tar.xz" ${LIBEVDEV} "http://www.freedesktop.org/software/libevdev"
fpkg -m xorg -s xserver ${XORG_SERVER}
fpkg -m xorg -s app ${XDM}
fpkg ${MTDEV} "http://bitmath.org/code/mtdev"

fpkg -e "tgz" ${XTERM} "ftp://invisible-island.net/xterm"
fpkg -e "tar.gz" ${T1LIB} "http://www.ibiblio.org/pub/Linux/libs/graphics"

fpkg -m sf -e "tar.bz2" -s "dejavu" ${DEJAVUFONTS}

fpkg -m sf -s "tcl" ${TK}

fpkg ${XDG_UTILS} "http://portland.freedesktop.org/download"
fpkg -e "tar.gz" ${WINDOWMAKER} ${WMAKER_URL}
fpkg -e "tar.gz" ${WINDOWMAKER_EXTRA} ${WMAKER_URL}

exit $?
